<?php

namespace Drupal\admin_toolbar_toggle;

class AdminToolbarToggle implements \Drupal\Core\Security\TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * Attach AdminToolbarToggle settings.
   */
  public static function preRender($element) {
    $config = \Drupal::config('admin_toolbar_toggle.settings');
    $element['#attached']['drupalSettings']['admin_toolbar_toggle'] = ['key' => $config->get('key')];

    return $element;
  }

}
